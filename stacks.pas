unit Stacks; { Version 0.2 }
             { Permite trabajar con dos tipos de datos }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

Type
  StackType = ^StackNode;
  StackNode = Record
                next : StackType;
                Case byte of
                  6: (Relinfo : real);
                  1: (Chrinfo : char);
                end;

  Stack = OBJECT
     Nodes : StackType;
     CONSTRUCTOR NewStack;
     FUNCTION EmptyStack : boolean;
  End;

  StackReal = OBJECT(Stack)
     PROCEDURE Push (x: real);
     FUNCTION Pop : real;
     FUNCTION StackTop : real;
  End;

  StackChar = OBJECT(Stack)
     PROCEDURE Push (x: char);
     FUNCTION Pop : char;
     FUNCTION StackTop : char;
  End;

implementation

CONSTRUCTOR Stack.NewStack;
begin
  Self.Nodes := nil;
end;

FUNCTION Stack.EmptyStack : boolean;
begin
  EmptyStack := Self.Nodes = nil;
end;

FUNCTION StackReal.StackTop : real;
begin
  StackTop := Self.Nodes^.Relinfo;
end;

FUNCTION StackChar.StackTop : char;
begin
  StackTop := Self.Nodes^.Chrinfo;
end;

PROCEDURE StackReal.Push (x: real);
var AuxS : StackType;
begin
  { Crear nuevo nodo }
  New (AuxS);
  AuxS^.Relinfo := x;
  AuxS^.next := Nodes;
  Nodes := AuxS;
end;

PROCEDURE StackChar.Push (x: char);
var AuxS : StackType;
begin
  { Crear nuevo nodo }
  New (AuxS);
  AuxS^.Chrinfo := x;
  AuxS^.next := Nodes;
  Nodes := AuxS;
end;

FUNCTION StackReal.Pop : real;
var
  AuxS : StackType;
begin
  Pop := Self.Nodes^.Relinfo;
  AuxS := Nodes;
  Nodes := Nodes^.next;
  Dispose (AuxS);
end;

FUNCTION StackChar.Pop : char;
var
  AuxS : StackType;
begin
  Pop := Self.Nodes^.Chrinfo;
  AuxS := Nodes;
  Nodes := Nodes^.next;
  Dispose (AuxS);
end;

end.

