unit uEvalExpr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Stacks;

type

  { TfrmPrincipal }

  TfrmPrincipal = class(TForm)
    btnSiete: TButton;
    btnCero: TButton;
    btnParIzq: TButton;
    btnPunto: TButton;
    btnParDer: TButton;
    btnExp: TButton;
    btnDiv: TButton;
    btnMult: TButton;
    btnSuma: TButton;
    btnCalcular: TButton;
    btnOcho: TButton;
    btnResta: TButton;
    btnBorrar: TButton;
    btnBorrarTodo: TButton;
    btnNueve: TButton;
    btnCuatro: TButton;
    btnCinco: TButton;
    btnSeis: TButton;
    btnUno: TButton;
    btnDos: TButton;
    btnTres: TButton;
    Label1: TLabel;
    Label3: TLabel;
    txtInput: TEdit;
    txtOutput: TEdit;
    procedure btnBorrarClick(Sender: TObject);
    procedure btnBorrarTodoClick(Sender: TObject);
    procedure btnCalcularClick(Sender: TObject);
    procedure btnCeroClick(Sender: TObject);
    procedure btnCincoClick(Sender: TObject);
    procedure btnCuatroClick(Sender: TObject);
    procedure btnDivClick(Sender: TObject);
    procedure btnDosClick(Sender: TObject);
    procedure btnExpClick(Sender: TObject);
    procedure btnMultClick(Sender: TObject);
    procedure btnNueveClick(Sender: TObject);
    procedure btnOchoClick(Sender: TObject);
    procedure btnParDerClick(Sender: TObject);
    procedure btnParIzqClick(Sender: TObject);
    procedure btnPuntoClick(Sender: TObject);
    procedure btnRestaClick(Sender: TObject);
    procedure btnSeisClick(Sender: TObject);
    procedure btnSieteClick(Sender: TObject);
    procedure btnSumaClick(Sender: TObject);
    procedure btnTresClick(Sender: TObject);
    procedure btnUnoClick(Sender: TObject);
  private

  public

  end;
  IdentStr = string[16];
  IdentList = ^IdentNode;
  IdentNode = Record
                Iinfo : IdentStr;
                Vinfo : real;
                next  : IdentList;
              end;
  Ran = 1..7;
  Con = Set of Ran;
  Inf = Array [Ran] of Con;
const
   Operators = ['+', '-', '*', '/', '^', '(', ')'];
   Superior : Inf = ([1,2,3,4,5,7],[1,2,3,4,5,7],       {MAS Y MENOS}
                     [3,4,5,7],[3,4,5,7],               {POR Y DIVIDIDO}
                     [7],                               {EXPONENTE}
                     [7],                               {PARENTESIS IZQUIERDO}
                     [1,2,3,4,5]);                      {PARENTESIS DERECHO}
   PI = 3.1415926535897932384;
var
  frmPrincipal: TfrmPrincipal;
  infijo: String;
  contador: Integer;
  ExprInitError: byte;
  ExprCalcError: byte;
  Lista1: IdentList;

implementation

{$R *.lfm}

{ TfrmPrincipal }

//Retira los espacios y caracteres inválidos.
function TransformaExpresion(expr: string): string;
var
  validos, nuevaExpr, expr2: string;
  pos, valida: integer;
  letra: char;
begin
  validos := 'abcdefghijklmnopqrstuvwxyz0123456789.+-*/^()';
  expr2 := lowercase(expr); nuevaExpr:='';
  for pos := 1 to length(expr2) do
  begin
    letra := expr2[pos];
    for valida := 1 to length(validos) do
    begin
      if letra = validos[valida] then
      begin
        nuevaExpr := nuevaExpr + letra;
        break;
      end;
    end;
  end;
  Result := nuevaExpr;
end;

//Devuelve true si el argumento es un operador
function IsOperator(c: char): boolean;
begin
  IsOperator := false;
  if (c = '+') or (c = '-') or (c = '*') or (c = '/') or (c = '^')
  then IsOperator := true;
end;

//1. Dos o más operadores estén seguidos. Ejemplo: 2++4, 5-*3
function DobleTripleOperadorSeguido(expr: string): boolean;
var
  pos: integer;
  car1, car2, car3: char;
begin
  for pos := 1 to length(expr) do
  begin
     car1 := expr[pos]; //Extrae un carácter
     car2 := expr[pos + 1]; //Extrae el siguiente carácter

     //Compara si el carácter y el siguiente son operadores, dado el caso retorna true
     if IsOperator(car1) then
        if IsOperator(car2) then
        begin
          Result := true;
          exit;
        end;
  end;

  for pos := 1 to length(expr) - 1 do
  begin
    car1 := expr[pos]; //Extrae un carácter
    car2 := expr[pos + 1]; //Extrae el siguiente carácter
    car3 := expr[pos + 2]; //Extrae el siguiente carácter

    //Compara si el carácter y el siguiente son operadores, dado el caso retorna true
    if IsOperator(car1) then
       if IsOperator(car2) then
          if IsOperator(car3) then
          begin
            Result := true;
            exit;
          end;
  end;

  Result := false; //No encontró doble/triple operador seguido
end;

//2. Un operador seguido de un paréntesis que cierra. Ejemplo: 2-(4+)-7
function OperadorParentesisCierra(expr: string): boolean;
var
  pos: integer;
  car1: char;
begin
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos]; //Extrae un carácter

    //Compara si el primer carácter es operador y el siguiente es paréntesis que cierra
    if IsOperator(car1) then
       if (expr[pos + 1] = ')') then
       begin
         Result := true;
         exit;
       end;
  end;
  Result := false; //No encontró operador seguido de un paréntesis que cierra
end;

//3. Un paréntesis que abre seguido de un operador. Ejemplo: 2-(*3)
function ParentesisAbreOperador(expr: string): boolean;
var
  pos: integer;
  car2: char;
begin
  for pos := 1 to length(expr) do
  begin
    car2 := expr[pos + 1]; //Extrae el siguiente carácter

    //Compara si el primer carácter es paréntesis que abre y el siguiente es operador
    if expr[pos] = '(' then if IsOperator(car2) then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false; //No encontró paréntesis que abre seguido de un operador
end;

//4. Que los paréntesis estén desbalanceados. Ejemplo: 3-(2*4))
function ParentesisDesbalanceados(expr: string): boolean;
var
  parabre, parcierra,
  pos: integer;
  car1: char;
begin
  parabre := 0;
  parcierra := 0;
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos];
    if car1 = '(' then Inc(parabre);
    if car1 = ')' then Inc(parcierra);
  end;
  if parabre <> parcierra then
    Result := true
  else
    Result := false;
end;

//5. Que haya paréntesis vacío. Ejemplo: 2-()*3
function ParentesisVacio(expr: string): boolean;
var
  pos: integer;
begin
  //Compara si el primer carácter es paréntesis que abre y el siguiente es paréntesis que cierra
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] = '(') and (expr[pos + 1] = ')') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//6. Así estén balanceados los paréntesis no corresponde el que abre con el que cierra. Ejemplo: 2+3)-2*(4
function ParentesisBalanceIncorrecto(expr: string): boolean;
var
  balance, pos: integer;
  car1: char;
begin
  balance := 0;
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos]; //Extrae un carácter
    if car1 = '(' then Inc(balance);
    if car1 = ')' then Dec(balance);
    if balance < 0 then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//7. Un paréntesis que cierra y sigue un número o paréntesis que abre. Ejemplo: (3-5)7-(1+2)(3/6)
function ParentesisCierraNumero(expr: string): boolean;
var
  pos: integer;
  car2: char;
begin
  for pos := 1 to length(expr) do
  begin
    car2 := expr[pos + 1]; //Extrae el siguiente carácter
    //Compara si el primer carácter es paréntesis que cierra y el siguiente es número
    if (expr[pos] = ')') then
       if (car2 >= '0') and (car2 <= '9') then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//8. Un número seguido de un paréntesis que abre. Ejemplo: 7-2(5-6)
function NumeroParentesisAbre(expr: string): boolean;
var
  pos: integer;
  car1: char;
begin
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos]; //Extrae un carácter
    //Compara si el primer carácter es número y el siguiente es paréntesis que abre
    if (car1 >= '0') and (car1 <= '9') then
       if expr[pos + 1] = '(' then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//9. Doble punto en un número de tipo real. Ejemplo: 3-2..4+1 7-6.46.1+2
function DoblePuntoNumero(expr: string): boolean;
var
  totalpuntos, pos: integer;
  car1: char;
begin
  totalpuntos := 0;
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos]; //Extrae un carácter
    if ((car1 < '0') or (car1 > '9')) and (car1 <> '.') then totalpuntos := 0;
    if (car1 = '.') then Inc(totalpuntos);
    if (totalpuntos > 1) then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//10. Un paréntesis que cierra seguido de una variable. Ejemplo: (12-4)y-1
function ParentesisCierraVariable(expr: string): boolean;
var
  pos: integer;
begin
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] = ')') then //Compara si el primer carácter es paréntesis que cierra y el siguiente es letra
       if (expr[pos + 1] >= 'a') and (expr[pos + 1] <= 'z') then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//11. Una variable seguida de un punto. Ejemplo: 4-z.1+3
function VariableluegoPunto(expr: string): boolean;
var
  pos: integer;
begin
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] >= 'a') and (expr[pos] <= 'z') then
       if expr[pos + 1] = '.' then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//12. Un punto seguido de una variable. Ejemplo: 7-2.p+1
function PuntoluegoVariable(expr: string): boolean;
var
  pos: integer;
begin
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] = '.') then
       if (expr[pos + 1] >= 'a') and (expr[pos + 1] <= 'z') then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//13. Un número antes de una variable. Ejemplo: 3x+1
//Nota: Algebraicamente es aceptable 3x+1 pero entonces vuelve más complejo un evaluador porque debe saber que 3x+1 es en realidad 3*x+1
function NumeroAntesVariable(expr: string): boolean;
var
  pos: integer;
begin
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] >= '0') and (expr[pos] <= '9') then
       if (expr[pos + 1] >= 'a') and (expr[pos + 1] <= 'z') then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//14. Un número después de una variable. Ejemplo: x21+4
function VariableDespuesNumero(expr: string): boolean;
var
  pos: integer;
begin
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] >= 'a') and (expr[pos] <= 'z') then
       if (expr[pos + 1] >= '0') and (expr[pos + 1] <= '9') then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

function IsFunction (var s : IdentStr): boolean;
begin
  IsFunction := (s = 'sen') or (s = 'cos') or (s = 'tan') or
                (s = 'exp') or (s = 'ln' ) or (s = 'log') or
                (s = 'asn') or (s = 'atn') or (s = 'acs') or
                (s = 'ent') or (s = 'abs') or (s = 'fac') or
                (s = 'raiz') or (s = 'sin');
end;

//15. Chequea si hay 4 o más variables seguidas
function Chequea4letras(expr: string): boolean;
var
  pos: integer;
  cad: IdentStr;
  car1, car2, car3, car4: char;
begin
  for pos := 1 to length(expr)-3 do
  begin
    car1 := expr[pos];
    car2 := expr[pos + 1];
    car3 := expr[pos + 2];
    car4 := expr[pos + 3];
    if (car1 >= 'a') and (car1 <= 'z')
        and (car2 >= 'a') and (car2 <= 'z')
        and (car3 >= 'a') and (car3 <= 'z')
        and (car4 >= 'a') and (car4 <= 'z') then
    begin
      cad := copy(expr,pos,4);
      if (not IsFunction(cad)) then
      begin
        Result := true;
        Exit;
      end;
    end;
  end;
  Result := false;
end;

//Chequea si las tres letras enviadas son una función
function EsFuncionInvalida(car1: char; car2: char; car3: char): boolean;
var
  pos, tamfunciones:integer;
  listfunc1, listfunc2, listfunc3: char;
  listafunciones: string;
begin
  listafunciones := 'sinsencostanabsasnacsatnlogceiexpsqrrcb';
  tamfunciones := length(listafunciones);
  pos := 1;
  while (pos <= tamfunciones - 2) do
  begin
    listfunc1 := listafunciones[pos];
    listfunc2 := listafunciones[pos + 1];
    listfunc3 := listafunciones[pos + 2];
    if (car1 = listfunc1) and (car2 = listfunc2) and (car3 = listfunc3) then
    begin
      Result := false;
      exit;
    end;
    pos := pos + 3;
  end;
  Result := true;
end;

//16. Si detecta tres letras seguidas y luego un paréntesis que abre, entonces verifica si es función o no
function FuncionInvalida(expr: string): boolean;
var
  pos: integer;
  car1, car2, car3: char;
begin
  for pos := 1 to length(expr)-2 do
  begin
    car1 := expr[pos];
    car2 := expr[pos + 1];
    car3 := expr[pos + 2];
  end;
  //Si encuentra tres letras seguidas
  if (car1 >= 'a') and (car1 <= 'z') and (car2 >= 'a') and (car2 <= 'z') and (car3 >= 'a') and (car3 <= 'z') then
  begin
    if pos >= length(expr) - 4 then begin Result:= true; Exit; end; //Hay un error porque no sigue paréntesis
    if expr[pos + 3] <> '(' then begin Result:= true; Exit; end; //Hay un error porque no hay paréntesis
    if EsFuncionInvalida(car1, car2, car3) then begin Result:= true; Exit; end;
  end;
  Result := false;
end;

//17. Si detecta sólo dos letras seguidas es un error
function VariableInvalida(expr: string): boolean;
var
  cuentaletras, pos: integer;
begin
  cuentaletras := 0;
  for pos := 1 to length(expr) do
  begin
    if (expr[pos] >= 'a') and (expr[pos] <= 'z') then
      Inc(cuentaletras)
    else
      begin
        if cuentaletras = 2 then
        begin
          Result := true;
          exit;
        end;
        cuentaletras := 0;
      end;
  end;
  if cuentaletras = 2 then
    Result := true
  else
    Result := false;
end;

//18. Antes de paréntesis que abre hay una letra function
function VariableParentesisAbre(expr: string): boolean;
var
  pos, cuentaletras: integer;
  car1: char;
begin
  cuentaletras := 0;
  for pos := 1 to length(expr) do
  begin
    car1 := expr[pos];
    if (car1 >= 'a') and (car1 <= 'z') then
      Inc(cuentaletras)
    else if (car1 = '(') and (cuentaletras = 1) then
    begin
      Result := true;
      Exit;
    end
    else
      cuentaletras := 0;
  end;
  Result := false;
end;

//19. Después de paréntesis que cierra sigue paréntesis que abre. Ejemplo: (4-5)(2*x)
function ParCierraParAbre(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]=')') and (expr[pos+1]='(') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//20. Después de operador sigue un punto. Ejemplo: -.3+7
function OperadorPunto(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if IsOperator(expr[pos]) then
       if expr[pos+1]='.' then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//21. Después de paréntesis que abre sigue un punto. Ejemplo: 3*(.5+4)
function ParAbrePunto(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]='(') and (expr[pos+1]='.') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//22. Un punto seguido de un paréntesis que abre. Ejemplo: 7+3.(2+6)
function PuntoParAbre(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]='.') and (expr[pos+1]='(') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//23. Paréntesis cierra y sigue punto. Ejemplo: (4+5).7-2
function ParCierraPunto(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]=')') and (expr[pos+1]='.') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

//24. Punto seguido de operador. Ejemplo: 5.*9+1
function PuntoOperador(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]='.') then
       if IsOperator(expr[pos+1]) then
       begin
         Result := true;
         Exit;
       end;
  end;
  Result := false;
end;

//25. Punto y sigue paréntesis que cierra. Ejemplo: (3+2.)*5
function PuntoParCierra(expr: string): boolean;
var
  pos: integer;
begin
  for pos:=1 to length(expr) - 1 do
  begin
    if (expr[pos]='.') and (expr[pos+1]=')') then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := false;
end;

function Numero (op : char): Ran;
begin
  case op of
    '+' : Numero := 1;
    '-' : Numero := 2;
    '*' : Numero := 3;
    '/' : Numero := 4;
    '^' : Numero := 5;
    '(' : Numero := 6;
    else  Numero := 7;
  end; {case}
end;

procedure FindNextPar (E : String; i1 : byte; var i2 : byte);
var
  index : byte;
  P     : StackChar;
  aux   : char;
  f1    : boolean;
begin
  P.NewStack;
  index := i1; f1 := true;
  while f1 do
  begin
    if E[index] = '(' then
      P.Push(E[index])
    else
      if E[index] = ')' then
        aux := P.Pop;
    f1 := not P.EmptyStack;
    Inc (index);
    if (index > length(E)) and f1 then
    begin
      i2 := 0;
      Exit;
    end;
  end;
  i2 := index-1;
end;

procedure InitExpr(var s : String);
{ Conversión de infijo a posfijo }
Var
  ExpEntr,
  ExpPost,
  SubExp     : String;
  L, ContPost,
  ContEntr,
  ContIdent,
  ContOper,
  Anter, Actual,
  Current    : byte;
  Simbol, Aux: char;
  PilaOper   : StackChar;
  flag       : Boolean;
  Ident      : IdentStr;
begin
  ExpEntr := s;
  ExpPost := '';
  L := length(ExpEntr);
  if L = 0 then
  begin
    ExprInitError := 4;
    Exit;
  end;
  { Conversión }
  PilaOper.NewStack;
  ContPost := 0;
  ContEntr := 1;
  ContOper := 0;
  ContIdent := 0;
  while ContEntr <= L do
  begin
    { Determinar identificadores }
    Anter := ContEntr; Actual := ContEntr; flag := true;
    while not(ExpEntr[ContEntr] in Operators) and flag do
    begin
      Inc (Actual);
      Inc (ContEntr);
      flag := ContEntr <= L;
    end;
    if Actual > Anter then
    begin
      ContIdent := ContIdent + 1;
      Ident := Copy (ExpEntr, Anter, Actual-Anter);
      if IsFunction (Ident) then
      begin
        if ExpEntr[Actual] <> '(' then
        begin
          ExprInitError := 3;
          Exit;
        end;
        FindNextPar (ExpEntr, Actual, Current);
        if Current = 0 then
        begin
          ExprInitError := 3;
          Exit;
        end;
        SubExp := Copy (ExpEntr, Actual+1, Current-Actual-1);
        InitExpr (SubExp);
        If ExprInitError <> 0 then Exit;
        Inc (ContPost,Actual-Anter+4+length(SubExp));
        ExpPost := ExpPost+'|'+Ident+'['+SubExp+']'+'|';
        Actual := Current + 1;
        ContEntr := Actual;
        flag := ContEntr <= L;
      end
      else
      begin
        Inc (ContPost, Actual-Anter+2);
        ExpPost := ExpPost + '|' + Ident + '|';
      end;
    end;
    Simbol := ExpEntr[ContEntr];
    if (Simbol = '-') and
       ((ExpEntr[ContEntr-1] = '(') or (ContEntr = 1)) then
    begin
      Inc (ContPost, 3);
      Inc (ContIdent);
      ExpPost := ExpPost + '|0|';
    end;
    if (Numero(Simbol) in [1..5]) and
       (Numero(ExpEntr[ContEntr-1]) in [1..5]) then
    begin
      ExprInitError := 2;
      Exit;
    end;
    while not PilaOper.EmptyStack and
          (Numero(PilaOper.StackTop) in Superior[Numero(Simbol)]) do
    begin
      Aux:=PilaOper.Pop;
      if not (Aux in ['(',')']) then
      begin
        Inc (ContPost);
        ExpPost := ExpPost + Aux;
      end;
    end;
    if flag then
    begin
      if (simbol = ')') and Not PilaOper.EmptyStack and
         (PilaOper.StackTop = '(') then
         Aux:=PilaOper.Pop
      else
      begin
        PilaOper.Push(simbol);
        if simbol <> ')' then Inc (ContOper);
      end;
      Inc (ContEntr);
    end;
  end;
  while Not PilaOper.EmptyStack do
  begin
    Aux := PilaOper.Pop;
    If not (Aux in ['(',')']) then
    begin
      Inc (ContPost);
      ExpPost := ExpPost + Aux;
    end
  end;
  s := ExpPost;
end;

procedure FindPipe (s : String; i1 : byte; var i2: byte);
var
  PilaBraq : StackChar;
  Aux      : char;
  flag     : boolean;
begin
  PilaBraq.NewStack;
  flag := s[i1] <> '|';
  while flag do
  begin
    Inc(i1);
    if s[i1] = '[' then PilaBraq.Push(s[i1]);
    if s[i1] = ']' then aux := PilaBraq.Pop;
    flag := not ((s[i1] = '|') and PilaBraq.EmptyStack);
  end;
  i2 := i1;
end;

function Operacion (V1,V2 : real; Oper : char): real;
var AuxV  : integer;
    AuxV2 : real;
begin
  Case Oper of
    '+' : Operacion := V1+V2;
    '-' : Operacion := V1-V2;
    '*' : Operacion := V1*V2;
    '/' : if V2 <> 0 then
            Operacion := V1/V2
          else
          begin
            Operacion := 0;
            ExprCalcError := 2;
          end;
    '^' : if V1 > 0 then
            Operacion := exp(V2*ln(V1))
          else
            if V1 < 0 then
            begin
              AuxV := Round(V2);
              if (AuxV = V2) then
              begin
                AuxV2 := exp(V2*ln(-V1));
                if (AuxV mod 2) = 0 then
                  Operacion := AuxV2
                else
                  Operacion := -AuxV2;
              end
              else
              begin
                Operacion := 0;
                ExprCalcError := 2;
              end;
            end
            else
              if V2 <> 0 then
                Operacion := 0
              else
              begin
                Operacion := 0;
                ExprCalcError := 2;
              end;
  end; {case}
end;

function tan (x : real): real;
begin
  x := x * PI / 180;
  if cos(x) <> 0 then
     tan := sin(x) / cos(x)
  else
  begin
    tan := 0;
    ExprCalcError := 2;
  end;
end;

function raiz (x: real): real;
begin
  if x >= 0 then
    raiz := Sqrt(x)
  else
  begin
    raiz := 0;
    ExprCalcError := 2;
  end;
end;

function fac (x : real): word;
var n,c,r : word;
begin
  if (x < MaxInt) and (x >= 0) then
  begin
    n := trunc(x); r := 1;
    for c := 1 to n do
      r := r*c;
    fac := r;
  end
  else
  begin
    fac := 0;
    ExprCalcError := 2;
  end;
end;

function asn (x : real): real;
var
    res: real;
begin
  if abs(x) < 1 then
  begin
    res := arctan(x/sqrt(1-x*x));
    asn := res * 180 / PI;
  end
  else
  begin
    asn := 0;
    ExprCalcError := 2;
  end;
end;

function acs (x : real): real;
var
    res: real;
begin
  if (abs(x) < 1) and (x <> 0) then
  begin
    res := arctan(sqrt(1-x*x)/x);
    acs := res * 180 / PI;
  end
  else
  begin
    acs := 0;
    ExprCalcError := 2;
  end;
end;

function ent (x : real): integer;
begin
  if (x < MaxInt) and (x > -MaxInt) then
    ent := trunc(x)
  else
  begin
    ent := 0;
    ExprCalcError := 2;
  end;
end;

function ResultOf (s : String): real; forward;

function ValueOf (s : String): real;
var code     : integer;
    aux, res : real;
    br1,
    br2,
    index    : byte;
    f1, f2   : boolean;
    sube,
    func     : String;
    PilaBraq : StackChar;
    ch       : char;
    LAux     : IdentList;
begin
  index := 1; f1 := index = length(s); f2 := s[index] <> ']';
  while not f1 and f2 do
  begin
    f2 := (s[index] <> '[');
    f1 := (index = length(s));
    Inc (index);
  end;
  if not f2 then
  begin
    PilaBraq.NewStack;
    br1 := index; f1 := true;
    { Buscar segundo braquet }
    Dec (index);
    while f1 do
    begin
      if s[index] = '[' then PilaBraq.Push(ch)
      else
        if s[index] = ']' then ch := PilaBraq.Pop;
      Inc (index);
      f1 := not PilaBraq.EmptyStack;
    end;
    br2 := index;
    sube := Copy(s,br1,br2-4);
    func := Copy(s,1,br1-2);
    aux := ResultOf(sube);
    if (func = 'sen') or (func = 'sin') then
      begin
        aux := aux * PI / 180;
        res := sin(aux)
      end
    else if func = 'cos' then
      begin
        aux := aux * PI / 180;
        res := cos(aux)
      end
    else if func = 'tan' then
      res := tan(aux)
    else if func = 'exp' then
      res := exp(aux)
    else if func = 'ln' then
         begin
           if aux > 0 then
             res := ln(aux)
           else
             ExprCalcError := 2
         end
         else if func = 'log' then
            res := ln(aux)/ln(10)
         else if func = 'raiz' then
            res := raiz(aux)
         else if func = 'atn' then
            begin
              res := arctan(aux);
              res := res * 180 / PI
            end
         else if func = 'asn' then
            begin
              res := asn(aux);
              { res := res * 180 / PI }
            end
         else if func = 'acs' then
            begin
              res := acs(aux);
              res := res * 180 / PI
            end
         else if func = 'ent' then
            res := ent(aux)
         else if func = 'abs' then
            res := abs(aux)
         else if func = 'fac' then
            res := fac(aux);
  end
  else
  begin
    Val (s,aux,code);
    if code = 0 then
      res := aux
    else
    begin
      { Buscar Identificadores }
      LAux := Lista1;
      while (LAux^.Iinfo <> s) and (LAux <> nil) do
        LAux := LAux^.next;
      if LAux <> nil then
        res := LAux^.Vinfo
      else
      begin
        res := 0;
        ExprCalcError := 3;
      end;
    end;
  end;
  ValueOf := res;
end;

function ResultOf (s : String): real;
{Evaluación de una expresión (INICIALIZADA!!!)}
var
  index, last, L : byte;
  valor, v1, v2  : real;
  PilaValores    : StackReal;
  Operand        : String;
begin
  ExprCalcError := 0;
  PilaValores.NewStack;
  index := 1;
  L := length(s);
  repeat
    { colocar index en un identificador }
    while s[index] = '|' do
      Inc(index);
    FindPipe (s,index,last);
    Operand := Copy (s, index, last-index);
    valor := ValueOf (Operand);
    if ExprCalcError <> 0 then Exit;
    PilaValores.Push(Valor);
    index := last;
    Inc (index);
    while (s[index] in Operators) and (index <= L) do
    begin
      V1 := PilaValores.Pop;
      V2 := PilaValores.Pop;
      valor := Operacion (V2,V1,s[index]);
      PilaValores.Push(valor);
      Inc (index);
    end;
  until index >= L;
  Valor := PilaValores.Pop;;
  ResultOf := Valor;
end;

procedure TfrmPrincipal.btnCeroClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '0';
end;

procedure TfrmPrincipal.btnBorrarClick(Sender: TObject);
var
  car: String[1];
begin
  car := Copy(txtInput.Text,Length(txtInput.Text)-1,1);
  txtInput.Text := Copy(txtInput.Text,1,Length(txtInput.Text)-1);
  If car = '.' Then
     contador := 0;
end;

procedure TfrmPrincipal.btnBorrarTodoClick(Sender: TObject);
begin
  txtInput.Text := '';
  txtOutput.Text:= '';
  txtInput.SetFocus();
end;

procedure TfrmPrincipal.btnCalcularClick(Sender: TObject);
begin
  //Primero comprobar la sintaxis de la expresión de entrada
  infijo := TransformaExpresion(txtInput.Text);
  if DobleTripleOperadorSeguido(infijo) then
     ShowMessage('Error de sintaxis: Dos o más operadores están seguidos. Ejemplo: 2++4, 5-*3')
  else if OperadorParentesisCierra(infijo) then
     ShowMessage('Error de sintaxis: Un operador seguido de un paréntesis que cierra. Ejemplo: 2-(4+)-7')
  else if ParentesisAbreOperador(infijo) then
     ShowMessage('Error de sintaxis: Un paréntesis que abre seguido de un operador. Ejemplo: 2-(*3)')
  else if ParentesisDesbalanceados(infijo) then
     ShowMessage('Error de sintaxis: Los paréntesis están desbalanceados. Ejemplo: 3-(2*4))')
  else if ParentesisVacio(infijo) then
     ShowMessage('Error de sintaxis: Paréntesis vacío. Ejemplo: 2-()*3')
  else if ParentesisBalanceIncorrecto(infijo) then
     ShowMessage('Error de sintaxis: Paréntesis que abre no corresponde con el que cierra. Ejemplo: 2+3)-2*(4')
  else if ParentesisCierraNumero(infijo) then
     ShowMessage('Error de sintaxis: Un paréntesis que cierra y sigue un número. Ejemplo: (3-5)7-(1+2)')
  else if NumeroParentesisAbre(infijo) then
     ShowMessage('Error de sintaxis: Un número seguido de un paréntesis que abre. Ejemplo: 7-2(5-6)')
  else if DoblePuntoNumero(infijo) then
     ShowMessage('Error de sintaxis: Doble punto en un número de tipo real. Ejemplos: 3-2..4+1 o 7-6.46.1+2')
  else if ParentesisCierraVariable(infijo) then
     ShowMessage('Error de sintaxis: Un paréntesis que cierra seguido de una variable. Ejemplo: (12-4)y-1')
  else if VariableluegoPunto(infijo) then
     ShowMessage('Error de sintaxis: Una variable seguida de un punto. Ejemplo: 4-z.1+3')
  else if PuntoluegoVariable(infijo) then
     ShowMessage('Error de sintaxis: Un punto seguido de una variable. Ejemplo: 7-2.p+1')
  else if NumeroAntesVariable(infijo) then
     ShowMessage('Error de sintaxis: Un número antes de una variable. Ejemplo: 3x+1')
  else if VariableDespuesNumero(infijo) then
     ShowMessage('Error de sintaxis: Un número después de una variable. Ejemplo: x21+4')
  else if Chequea4letras(infijo) then
     ShowMessage('Error de sintaxis: Hay 4 o más letras seguidas. Ejemplo: 12+ramp+8.9')
  else if FuncionInvalida(infijo) then
     ShowMessage('Error de sintaxis: Función inexistente. Ejemplo: 5*alo(78)')
  else if VariableInvalida(infijo) then
     ShowMessage('Error de sintaxis: Variable inválida (solo pueden tener una letra). Ejemplo: 5+tr-xc+5')
  else if VariableParentesisAbre(infijo) then
     ShowMessage('Error de sintaxis: Variable seguida de paréntesis que abre. Ejemplo: 5-a(7+3)')
  else if ParCierraParAbre(infijo) then
     ShowMessage('Error de sintaxis: Después de paréntesis que cierra sigue paréntesis que abre. Ejemplo: (4-5)(2*x)')
  else if OperadorPunto(infijo) then
     ShowMessage('Error de sintaxis: Después de operador sigue un punto. Ejemplo: -.3+7')
  else if ParAbrePunto(infijo) then
     ShowMessage('Error de sintaxis: Después de paréntesis que abre sigue un punto. Ejemplo: 3*(.5+4)')
  else if PuntoParAbre(infijo) then
     ShowMessage('Error de sintaxis: Un punto seguido de un paréntesis que abre. Ejemplo: 7+3.(2+6)')
  else if ParCierraPunto(infijo) then
     ShowMessage('Error de sintaxis: Paréntesis cierra y sigue punto. Ejemplo: (4+5).7-2')
  else if PuntoOperador(infijo) then
     ShowMessage('Error de sintaxis: Punto seguido de operador. Ejemplo: 5.*9+1')
  else if PuntoParCierra(infijo) then
     ShowMessage('Error de sintaxis: Punto seguido de paréntesis que cierra. Ejemplo: (3+2.)*5')
  else { Si ha llegado hasta aquí quiere decir que no existen errores de sintaxis y se procede a la conversión }
     begin
       { Primero "inicializamos" la expresión... }
       InitExpr(infijo);
       Case ExprInitError Of
         4: ShowMessage('Expresión vacía!');
         Else
           { Aquí va el código de evaluación de la expresión posfija }
           txtOutput.Text := FloatToStr(ResultOf(infijo));
       end;
     end;
end;

procedure TfrmPrincipal.btnCincoClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '5';
end;

procedure TfrmPrincipal.btnCuatroClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '4';
end;

procedure TfrmPrincipal.btnDivClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '/';
end;

procedure TfrmPrincipal.btnDosClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '2';
end;

procedure TfrmPrincipal.btnExpClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '^';
end;

procedure TfrmPrincipal.btnMultClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '*';
end;

procedure TfrmPrincipal.btnNueveClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '9';
end;

procedure TfrmPrincipal.btnOchoClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '8';
end;

procedure TfrmPrincipal.btnParDerClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + ')';
end;

procedure TfrmPrincipal.btnParIzqClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '(';
end;

procedure TfrmPrincipal.btnPuntoClick(Sender: TObject);
begin
  If contador = 0 Then
     begin
        txtInput.Text := txtInput.Text + '.';
        contador := contador + 1;
     end;
end;

procedure TfrmPrincipal.btnRestaClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '-';
end;

procedure TfrmPrincipal.btnSeisClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '6';
end;

procedure TfrmPrincipal.btnSieteClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '7';
end;

procedure TfrmPrincipal.btnSumaClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '+';
end;

procedure TfrmPrincipal.btnTresClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '3';
end;

procedure TfrmPrincipal.btnUnoClick(Sender: TObject);
begin
  txtInput.Text := txtInput.Text + '1';
end;

end.

